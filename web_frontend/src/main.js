import Vue from "vue";
import App from "./App";
import router from "./router/index";

import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";
import firebase from "firebase";
import cors from 'cors';


firebase.initializeApp(
  {apiKey: "AIzaSyBEc4saDBV8QChEbTXh7TdZp8KquQ47Pdc",
  authDomain: "cem-an-a.firebaseapp.com",
  projectId: "cem-an-a",
  storageBucket: "cem-an-a.appspot.com",
  messagingSenderId: "529725718542",
  appId: "1:529725718542:web:b2f2e9099f08d98dc37c5f",
  measurementId: "G-JE9B302S1Q"}
);

Vue.use(PaperDashboard);
Vue.use(cors);

/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
