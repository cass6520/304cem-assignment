import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Pages
import Product from "@/pages/Product.vue";
import Home from "@/pages/Home.vue";
import StoreMap from "@/pages/StoreMap.vue";
import Login from "@/pages/Login.vue";
import Register from "@/pages/Register.vue";
import ManageProduct from "@/pages/ManageProduct.vue";
import AddProductPage from "@/pages/AddProductPage.vue";
import ManageProductPage from "@/pages/ManageProductPage.vue";
import ManageProfile from "@/pages/ManageProfile.vue";
import ChangePassword from "@/pages/ChangePassword.vue";
import ManageStorePage from "@/pages/ManageStorePage.vue";
import ManageStore from "@/pages/ManageStore.vue";
import AddStorePage from "@/pages/AddStorePage.vue";
import Logout from "@/pages/Logout.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/home",
    children: [
      {
        path: "product",
        name: "Product",
        component: Product
      },
      {
        path: "maps",
        name: "store",
        component: StoreMap
      },
      {
        path: "login",
        name: "login",
        component: Login
      },
      {
        path: "register",
        name: "register account",
        component: Register
      },
      {
        path: "home",
        name: "home",
        component: Home
      },
      {
        path: "manageproduct",
        name: "manage product",
        component: ManageProduct
      },
      {
        path: "addproduct",
        name: "add product",
        component: AddProductPage
      },
      {
        path: "manageproductpage",
        name: "manage product page",
        component: ManageProductPage
      },
      {
        path: "manageprofile",
        name: "manage profile",
        component: ManageProfile
      },
      {
        path:"changepassword",
        name: "change password",
        component: ChangePassword
      },
      {
        path: "managestorepage",
        name: "manage store page",
        component: ManageStorePage
      },
      {
        path: "managestore",
        name: "manage store",
        component: ManageStore
      },
      {
        path: "addstore",
        name: "add store",
        component: AddStorePage
      },
      {
        path: "logout",
        name: "Logout",
        component: Logout
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
