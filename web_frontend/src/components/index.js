import PaperTable from "./PaperTable.vue";
import Button from "./Button";

import Card from "./Cards/Card.vue";

import SidebarPlugin from "./SidebarPlugin/index";

let components = {
  Card,
  PaperTable,
  SidebarPlugin
};

export default components;

export {
  Card,
  PaperTable,
  Button,
  SidebarPlugin,
};
