const express = require('express');
const ProductModel = require('../models/ProductModel');
const router = express.Router();


//Get All the Product
router.get('/', async (req, res) => {
    try {
        const posts = await ProductModel.find();
        res.json(posts);
    } catch (err) {
        res.json({
            message: err
        });
    }
});

//Add a New Product
router.post('/', async (req, res) => {
    const { product_name, product_price, product_photo } = req.body;
    let product = await ProductModel.findOne({ product_name });
    if (product) {
        return res.status(400).json({
            msg: "Product already exist"
        });
    }
    const command = new ProductModel({
        product_name: req.body.product_name,
        product_price: req.body.product_price,
        product_photo: req.body.product_photo
    });
    try {
        const savedCommand = await command.save()
        res.json(savedCommand)
    } catch (err) {
        res.json({message: err});
    }
});


//Post to Get Specific Product 
router.post('/getproduct', async (req, res) => {
    try {
        const {
            product_name
        } = req.body;
        let product = await ProductModel.findOne({
            product_name: product_name
        });
        if (product) {
            res.json(product)
        } else {
            return res.status(400).json({
                msg: "Error!"
            });
        }
    } catch (err) {
        res.json({
            message: err
        });
    }
});


//Get Specific Product
router.get('/:id', async (req, res) => {
    try {
        const post = await ProductModel.findById(req.params.id);
        res.json(post);
    } catch (err) {
        res.json({
            message: err
        });
    }
});



//Delete a Product
router.delete('/:id', async (req, res) => {
    try {
        const removedPost = await ProductModel.remove({
            _id: req.params.id
        })
        res.json(removedPost);
    } catch (err) {
        res.json({
            message: err
        });
    }
});

//Update Product Information
router.put('/:id', async (req, res) => {
    try {
        const updatedPost = await ProductModel.updateOne({
            _id: req.params.id
        }, {
            $set: {
                product_name: req.body.product_name,
                product_price: req.body.product_price,
                product_photo: req.body.product_photo
            }
        });
        res.json(updatedPost);
    } catch (err) {
        res.json({
            message: err
        });
    }
})

module.exports = router;