const express = require('express');
const AccountModel = require('../models/AccountModel');
const router = express.Router();
var passwordHash = require('password-hash');

//Get All the Account
router.get('/', async (req, res) => {
    try{
         const posts = await AccountModel.find();
         res.json(posts);
    }catch(err){
        res.json({message: err});
    }
});

//Register User Account
router.post('/', async (req,res) => {
    const { account_username, account_password, account_email } = req.body;
    let account = await AccountModel.findOne({ account_username });
        if (account) {
            return res.status(400).json({msg: "Account already exist!"});
        }
    const e_password = passwordHash.generate(account_password);
    const comment = new AccountModel({
        account_username: req.body.account_username,
        account_email: req.body.account_email,
        account_password: e_password,
        account_role: "user"
    });
    try{   
        const savedCommend = await comment.save()
        res.json(savedCommend);
    }catch (err) {
       res.json({message: err}); 
    }
});

//Register Admin Account
router.post('/admin', async (req,res) => {
    const { account_username, account_password } = req.body;
    let account = await AccountModel.findOne({ account_username });
        if (account) {
            return res.status(400).json({msg: "Account already exist!"});
        }
    const e_password = passwordHash.generate(account_password);
    const comment = new AccountModel({
        account_username: req.body.account_username,
        account_email: req.body.account_email,
        account_password: e_password,
        account_role: "admin"
    });
    try{   
        const savedCommend = await comment.save()
        res.json(savedCommend);
    }catch (err) {
       res.json({message: err}); 
    }
});

//Login – Check Username and Password
router.post('/login', async (req,res) => {
    try{  
        const { account_username, account_password } = req.body;
        let account = await AccountModel.findOne({ account_username: account_username });
        if (account_username != "" && account_password != ""){    
            if (account) {
                if (passwordHash.verify(account_password, account.account_password)){
                    res.json(account)
                } else {
                    return res.status(400).json({msg: "Password not match"});
                }
            } else {
                return res.status(401).json({msg: "No such user name"});
            }
        } else {
            return res.status(402).json({msg: "Please enter vaild username and password"});
        }
    }catch (err) {
       res.json({message: err}); 
    }
});

//Manage Account - Get Account Data
router.get('/:id', async (req,res) => {
    try {
         const post = await AccountModel.findById(req.params.id);
         res.json(post);
    }catch (err) {
        res.json({message: err});
    }    
});

//Manage Account - Change Account Data
router.put('/:id', async (req,res) => {
    try{
    const updatedPost = await AccountModel.updateOne(
        {_id: req.params.id}, 
        {$set: {account_email: req.body.account_email,
                account_username: req.body.account_username,
                account_password: req.body.account_password,
                account_role: req.body.account_role
        }}
    );
    res.json(updatedPost);    
    }catch (err) {
    res.json({message: err});
    }
})

//Change Password - Check the Old Password is Matching
router.post('/cpver', async (req,res) => {
    try{  
        const { account_username, old_password } = req.body;
        let account = await AccountModel.findOne({ account_username: account_username });
            if (account) {
                if (passwordHash.verify(old_password, account.account_password)){
                    res.json(account)
                } else {
                    return res.status(400).json({msg: ''});
                }
            } else {
                return res.status(400).json({msg: ''});
            }
    }catch (err) {
       res.json({message: err}); 
    }
});

//Change Password - Update the New Password
router.put('/cp/:id', async (req,res) => {
    try{
    const { account_username, account_password, account_email, account_role } = req.body;
    const e_password = passwordHash.generate(account_password);
    const updatedPost = await AccountModel.updateOne(
        {_id: req.params.id}, 
        {$set: {account_email: req.body.account_email,
                account_username: req.body.account_username,
                account_password: e_password,
                account_role: req.body.account_role
        }}
    );
    res.json(updatedPost);    
    }catch (err) {
    res.json({message: err});
    }
})

module.exports = router;