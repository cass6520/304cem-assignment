const express = require('express');
const router = express.Router();
const StoreInfoModel = require('../models/StoreInfoModel');


//Get All the Store
router.get('/', async (req, res) => {
    try{
         const posts = await StoreInfoModel.find();
         res.json(posts);
    }catch(err){
        res.json({message: err});
    }
});

//Add a New Store
router.post('/',async (req,res) => {
    const { name_eng, address_eng, latitude, longitude, storephoto } = req.body;
    let store = await StoreInfoModel.findOne({ name_eng });
    if (store) {
        return res.status(400).json({
            msg: "Store already exist"
        });
    }
    const command = new StoreInfoModel({
        name_eng: req.body.name_eng,
        address_eng: req.body.address_eng,
        latitude: req.body.latitude,
        longitude: req.body.longitude,
        storephoto: req.body.storephoto
    });
    try{   
        const savedCommand = await command.save()
        res.json(savedCommand)
    }catch (err) {
       res.json({message: err}); 
    }
});

//Post to Get Specific Store 
router.post('/getstore', async (req,res) => {
    try {
        const { name_eng } = req.body;
        let store = await StoreInfoModel.findOne({ name_eng: name_eng });
        if (store) {
            res.json(store)
        } else {
            return res.status(400).json({msg: "Error!"});
        }
    }catch (err) {
        res.json({message: err});
    }    
});

//Get Specific Store
router.get('/:id', async (req,res) => {
    try {
         const post = await StoreInfoModel.findById(req.params.id);
         res.json(post);
    }catch (err) {
        res.json({message: err});
    }    
});


//Delete Store
router.delete('/:id', async (req,res) => {
    try {
    const removedPost = await StoreInfoModel.remove({_id: req.params.id}) 
    res.json(removedPost);
    }catch (err) {
    res.json({message: err});
    }
});

//Update Store Information
router.put('/:id', async (req, res) => {
    try {
        const updatedPost = await StoreInfoModel.updateOne({
            _id: req.params.id
        }, {
            $set: {
                name_eng: req.body.name_eng,
                address_eng: req.body.address_eng,
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                storephoto: req.body.storephoto
            }
        });
        res.json(updatedPost);
    } catch (err) {
        res.json({
            message: err
        });
    }
})

module.exports = router;