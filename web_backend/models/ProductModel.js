const mongoose = require('mongoose');

const ProductPostSchema = mongoose.Schema({
    product_name: {
        type: String,
        require: true   
    },
    product_price: {
        type: String,
        require: true   
    },
    product_photo: {
        type: String,
        require: true   
    },
});

module.exports = mongoose.model('product', ProductPostSchema);