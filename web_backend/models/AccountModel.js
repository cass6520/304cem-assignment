'use strict';

const mongoose = require('mongoose');

const AccountPostSchema = mongoose.Schema({
    account_username: { 
        type: String,
        require: true
    },
    account_email:{
        type: String,
        require: true
    },
    account_password: { 
        type: String,
        require: true   
    },
    account_role: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('account', AccountPostSchema);