const mongoose = require('mongoose');

const StoreInfoPostSchema = mongoose.Schema({
    name_eng: { 
        type: String,
        require: true
    },
    address_eng: { 
        type: String,
        require: true   
    },
    latitude: { 
        type: String,
        require: true   
    },
    longitude: { 
        type: String,
        require: true   
    },
    storephoto: {
        type: String,
        require: true 
    }
});


module.exports = mongoose.model('storeinfo', StoreInfoPostSchema);