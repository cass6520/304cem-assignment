const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

app.use(bodyParser.json());
app.use(cors());


//Import Routes
app.use('/product', require('./routes/ProductRoute'));
app.use('/account', require('./routes/AccountRoute'));
app.use('/store', require('./routes/StoreInfoRoute'));

//Routes
app.get('/', (req,res) => {
    res.send('testing server');
});


//Connect to DB
mongoose.connect(
process.env.DB_CONNECTION,
{ useNewUrlParser: true }, 
() => console.log('connected to DB!')
);


//How to we start Listening the server
app.listen(3000);